<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('data');
    }

    function index() {
        $data['msg'] = '';
        $data['posting'] = $this->data->ambil_data()->result();
        $data['rows'] = $this->data->ambil_data()->num_rows();
        $this->load->view('header', $data);
        $this->load->view('content', $data);
        $this->load->view('sidebar', $data);
        $this->load->view('footer', $data);
    }

    function home() {
        $data['msg'] = '';
        $data['posting'] = $this->data->ambil_data()->result();
        $data['rows'] = $this->data->ambil_data()->num_rows();
        $this->load->view('header', $data);
        $this->load->view('home', $data);
        $this->load->view('sidebar', $data);
        $this->load->view('footer', $data);
    }

    function post() {
        $data['rows'] = $this->data->ambil_data()->num_rows();
        $data['post'] = $this->data->ambil_data()->result();
        $this->load->view('header', $data);
        $this->load->view('post', $data);
        $this->load->view('sidebar', $data);
        $this->load->view('footer', $data);
    }

//    function signup() {
//        $data['rows'] = $this->data->ambil_data()->num_rows();
//        $data['post'] = $this->data->ambil_data()->result();
//        $this->load->view('header', $data);
//        $this->load->view('signup', $data);
//        $this->load->view('sidebar', $data);
//        $this->load->view('footer', $data);
//    }

    function valid() {
        $this->form_validation->set_rules('username', 'username', 'required');
        $this->form_validation->set_rules('password', 'password', 'required');
        if ($this->form_validation->run() != false) {
            $username = $this->input->post('username');
            $password = $this->input->post('password');

            $where = array(
                'user_name' => $username,
                'password' => $password
            );

            $cek = $this->data->cek_login('user', $where)->num_rows();

            if ($cek > 0) {
                $data_session = array(
                    'user' => $username,
                    'status' => 'login'
                );
                $this->session->set_userdata($data_session);
                redirect(base_url());
            } else {
                $data['msg'] = 'failed';
                $data['posting'] = $this->data->ambil_data()->result();
                $data['rows'] = $this->data->ambil_data()->num_rows();
                $this->load->view('header', $data);
                $this->load->view('content', $data);
                $this->load->view('sidebar', $data);
                $this->load->view('footer', $data);
            }
        } else {
            $data['msg'] = '';
            $data['posting'] = $this->data->ambil_data()->result();
            $data['rows'] = $this->data->ambil_data()->num_rows();
            $this->load->view('header', $data);
            $this->load->view('content', $data);
            $this->load->view('sidebar', $data);
            $this->load->view('footer', $data);
        }
    }

    function logout() {
        $this->session->sess_destroy();
        redirect(base_url());
    }

}
