<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Forum extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('data');
    }

    function new_post() {
        $data['msg'] = '';
        $data['posting'] = $this->data->ambil_data()->result();
        $data['rows'] = $this->data->ambil_data()->num_rows();
        $this->load->view('header', $data);
        $this->load->view('new_post', $data);
        $this->load->view('sidebar', $data);
        $this->load->view('footer', $data);
    }

    function upload_post() {
        $this->form_validation->set_rules('category', 'category', 'required');
        $this->form_validation->set_rules('judul', 'judul', 'required');
        $this->form_validation->set_rules('image', 'image', 'required');
        $this->form_validation->set_rules('content', 'content', 'required');
        if ($this->form_validation->run() != false) {
            $category = $this->input->post('category');
            $judul = $this->input->post('judul');
            $image = $this->input->post('image');
            $content = $this->input->post('content');
            $time = time();

            $data = array(
                'p_id' => '',
                'p_category' => $category,
                'p_date' => $time,
                'p_title' => $judul,
                'u_id' => $this->session->user_data(),
                'p_img' => $image,
                'p_content' => $content
            );
        } else {
            $data['msg'] = '';
            $data['posting'] = $this->data->ambil_data()->result();
            $data['rows'] = $this->data->ambil_data()->num_rows();
            $this->load->view('header', $data);
            $this->load->view('new_post', $data);
            $this->load->view('sidebar', $data);
            $this->load->view('footer', $data);
        }
    }

}
