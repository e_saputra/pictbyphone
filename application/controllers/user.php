<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('data');
    }

    function index() {
        $data['msg']="";
        $this->load->view('header', $data);
        $this->load->view('signup', $data);
        $this->load->view('sidebar', $data);
        $this->load->view('footer', $data);
    }

    function signup() {
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->form_validation->set_rules('user_name', 'Username', 'required');
        $this->form_validation->set_rules('s_password', 'Password', 'required');
        $this->form_validation->set_rules('c_password', 'Confirmation Password', 'required|matches[password]');
        $this->form_validation->set_rules('email', 'E-mail', 'required|valid_email');
        $this->form_validation->set_rules('f_name', 'Fisrt Name', 'required');
        $this->form_validation->set_rules('l_name', 'Last Name', 'required');
        if ($this->form_validation->run() != false) {
            $user_id = '';
            $user_name = $this->input->post('user_name');print_r('masuk');exit;
            $password = $this->input->post('s_password');
            $email = $this->input->post('email');
            $f_name = $this->input->post('f_name');
            $l_name = $this->input->post('l_name');

            $where = array(
                'user_name' => $user_name
            );

            $cek = $this->data->cek_user('user', $where)->num_rows();
            if ($cek == 0) {

                $data = array(
                    'user_id' => $u_id,
                    'user_name' => $u_name,
                    'password' => $password,
                    'email' => $email,
                    'f_name' => $f_name,
                    'l_name' => $l_name
                );

                $this->data->signup($data, 'user');
                redirect(base_url());
            } else {
                $data['msg'] = 'Username already exist!!';
                $data['rows'] = $this->data->ambil_data()->num_rows();
                $data['post'] = $this->data->ambil_data()->result();
                $this->load->view('header', $data);
                $this->load->view('signup', $data);
                $this->load->view('sidebar', $data);
                $this->load->view('footer', $data);
            }
        } else {
            $user_name = $this->input->post('u_name');
            $where = array(
                'user_name' => $user_name
            );

            $cek = $this->data->cek_user('user', $where)->num_rows();

            $data['msg'] = ($cek == 0) ? '' : 'Username already exist!!';
            $data['rows'] = $this->data->ambil_data()->num_rows();
            $data['post'] = $this->data->ambil_data()->result();
            $this->load->view('header', $data);
            $this->load->view('signup', $data);
            $this->load->view('sidebar', $data);
            $this->load->view('footer', $data);
        }
    }

    function user_data() {
        if ($this->session->userdata('status') == 'login') {
            $data['msg'] = '';
            $data['rows'] = $this->data->ambil_data()->num_rows();
            $data['post'] = $this->data->ambil_data()->result();
            $this->load->view('header', $data);
            $this->load->view('user_data', $data);
            $this->load->view('sidebar', $data);
            $this->load->view('footer', $data);
        } else {
            redirect(base_url());
        }
    }

}
