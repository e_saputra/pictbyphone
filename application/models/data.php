<?php

class Data extends CI_Model{
	function ambil_data(){
		return $this->db->get('posting');
	}
        
        function cek_login($table, $where) {
            return $this->db->get_where($table,$where);
        }
        
        function cek_user($table, $where) {
            return $this->db->get_where($table,$where);
        }
        
        function signup($data, $table) {
        $this->db->insert($table, $data);
    }
}