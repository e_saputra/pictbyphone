<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>mencoba pengaturan css</title>
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/style.css">
        <script src="<?php echo base_url() ?>assets/js/jquery.min.js"></script>
        <script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
    </head>
    <body>
        <header>
            <div class="container">
                <h1><a href="<?php echo base_url() ?>">Pict<span>Byphone</span></a></h1>
            </div>        
        </header>
        <nav class="navbar navbar-default" role="navigation" style="z-index: 1">
            <div class="container">
                <div class="navbar-header">
                    <div class="navbar-brand" id="headerbrand"></div>
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mycol">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse" id="mycol">
                    <ul class="nav navbar-nav" id="mycol">
                        <li class="active"><a href="<?php echo base_url() . "main"?>">Home</a></li>
                        <li><a href="">Category</a></li>
                        <li><a href="#">What New</a></li>
                        <!--<li><a href="#">Page 3</a></li>-->
                    </ul>
                    <?php $user = ($this->session->userdata('status') == 'login') ? $this->session->userdata('user') : ''; ?>
                    <form class="navbar-form navbar-right" role="login" action="<?php echo base_url() . 'main/valid'; ?>" method="post">
                        <div class="form-group">
                            <?php echo (empty($user)) ? '<input type="text" class="form-control" name="username" placeholder="Username" size="10">' : ''; ?>
                            <?php echo (empty($user)) ? '<input type="password" class="form-control" name="password" placeholder="Password" size="10">' : ''; ?>
                            <?php echo (empty($user)) ? '<button type="submit" class="btn btn-default">Sign In</button>' : ''; ?>
                            <?php echo (empty($user)) ? '<a href= "http://pictbyphone.com/user" class="button">Sign Up</a>' : '' ?>
                            <?php echo (!empty($user)) ? '<i class="glyphicon glyphicon-user"></i>' : ''; ?>
                            <?php echo (!empty($user)) ? 'Hai <a href="http://pictbyphone.com/user/user_data" >' . $user . '</a>' : ''; ?>
                            <?php echo (!empty($user)) ? '<a href= "http://pictbyphone.com/main/logout" class="button">Logout</a>' : '' ?>
                        </div>
                    </form>
                </div>
            </div>
        </nav>
        <?php
        if (!empty(form_error('password')) && !empty(form_error('username'))) {
            echo '<center><div class="alert alert-warning fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Login form must be filled</div></center>';
        } else {
            if (empty(form_error('password'))) {
                echo '<center>' . form_error('username', '<div class="alert alert-warning fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>', '</div>') . '</center>';
            } else if (empty(form_error('username'))) {
                echo '<center>' . form_error('password', '<div class="alert alert-warning fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>', '</div>') . '</center>';
            }
        }
        if ($msg == 'failed'){
            echo '<center><div class="alert alert-warning fade in">login failed, username or password incorrect<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></div></center>';
        }
        ?>