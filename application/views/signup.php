<div class="container">
    <div class="row">
        <content class="col-md-8">
            <section>
            <form class="form-horizontal" action="<?php echo base_url()?>user/signup" method="post">
                <div class="form-group">
                    <label class="control-label col-sm-3" for="f_name" style="text-align: left">Firt Name</label>
                    <div class="col-xs-3">
                        <input type="text" class="form-control" name="f_name" value="<?php echo set_value('f_name'); ?>">
                    </div>
                    <div class="col-xs-5" style="color: red">
                        <?php echo form_error('f_name'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="l_name" style="text-align: left">Last Name</label>
                    <div class="col-xs-3">
                        <input type="text" class="form-control" name="l_name" value="<?php echo set_value('l_name'); ?>">
                    </div>
                    <div class="col-xs-5" style="color: red">
                        <?php echo form_error('l_name'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="u_name" style="text-align: left">User Name</label>
                    <div class="col-xs-3">
                        <input type="text" class="form-control" name="u_name" value="<?php echo set_value('u_name'); ?>">
                    </div>
                    <div class="col-xs-5 error" style="color: red">
                        <?php 
                        echo form_error('u_name'); 
                        echo $msg;
                        ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="email" style="text-align: left">Email Addres</label>
                    <div class="col-xs-3">
                        <input type="text" class="form-control" name="email" value="<?php echo set_value('email'); ?>">
                    </div>
                    <div class="col-xs-5" style="color: red">
                        <?php echo form_error('email'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="s_password" style="text-align: left">Password</label>
                    <div class="col-xs-3">
                        <input type="password" class="form-control" name="s_password" value="<?php echo set_value('s_password'); ?>">
                    </div>
                    <div class="col-xs-5" style="color: red">
                        <?php echo form_error('s_password'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="c_password" style="text-align: left">Password Confirmation</label>
                    <div class="col-xs-3">
                        <input type="password" class="form-control" name="c_password" value="<?php echo set_value('c_password'); ?>">
                    </div>
                    <div class="col-xs-5" style="color: red">
                        <?php echo form_error('c_password'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="signup">
                </div>
            </form>
            </section>
        </content>