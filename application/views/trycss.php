<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>mencoba pengaturan css</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>/assets/css/style.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="bootstrap.min.js"></script>
    </head>
    <body>
        <header>
            <div class="container">
                <h1><a href="<?php echo base_url() ?>">Just<span>Try</span></a></h1>
            </div>        
        </header>
        <div class="navbar navbar-default" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <!--                    <div class="navbar-brand" id="headerbrand">My template</div>-->
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mycol">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <?php $user = ($this->session->userdata('status') == 'login') ? $this->session->userdata('user') : ''; ?>
                <form class="navbar-form navbar-right" role="login" action="<?php echo base_url() . 'main/valid'; ?>" method="post">
                    <div class="form-group">
                        <?php echo (empty($user)) ? '<input type="text" class="form-control" name="username" placeholder="Username" size="10">' : ''; ?>
                        <?php echo (empty($user)) ? '<input type="password" class="form-control" name="password" placeholder="Password" size="10">' : ''; ?>
                        <?php echo (empty($user)) ? '<button type="submit" class="btn btn-default">Sign In</button>' : ''; ?>
                        <?php echo (!empty($user)) ? '<i class="glyphicon glyphicon-user"></i>' : ''; ?>
                        <?php echo (!empty($user)) ? 'Hai ' . $user : ''; ?>
                        <?php echo (!empty($user)) ? '<a href= "http://pictbyphone.com/main/logout" class="button">Logout</a>' : '' ?>
                        <?php echo validation_errors(); ?>
                    </div>
                </form>
                
            </div>
        </div>
        <div class="container">
            <div class="row">
                <content class="col-md-8">
                    <br><br>
                    <section>
                        <article class="expanded">
                            <h3>Some time in the shore</h3>
                            <image class="img-responsive" src="<?php echo base_url() ?>assets/img/Lighthouse.jpg">
                            <p align="justify">Kapolsek Benteng Kompol Ewo Samono membenarkan adanya peristiwa tersebut. Ewo mengatakan, korban selamat meski mengalami luka. "Korban sudah dibawa ke RS Awal Bros, hanya retak di bagian kaki saja," ujar Ewo kepada detikcom, Senin (16/1/2017). Peristiwa terjadi pada Minggu 15 Januari sekitar pukul 15.20 WIB. Saat berbelanja, Sriyatun mendapati anaknya dalam bahaya. "Anaknya maen di eskalator dan hendak terjatuh, kemudian ditolong korban," imbuhnya. Sriyatun berusaha menolong anaknya yang saat itu tergantung di eskalator. Namun nahas, Sriyatun malah terjatuh dari lantai 1 ke ground floor. Beruntung anak korban diselamatkan oleh seorang pengunjung mal lainnya. Sementara korban dilarikan ke RS Awal Bros.</p>
                            <a href="#" class="button">Read more</a>
                            <?php
                            if ($this->session->userdata('status') == 'login') {
                                echo '<a href="#" class="button">Comments</a>';
                            } else {
                                echo 'Please <a href="">login</a> to coment';
                            }
                            ?>
                        </article>
                    </section>
                </content>
                <aside class="col-md-4" border="1">
                    <br><br><br>

                    <h4>Categories</h4>
                    <ul>
                        <li><a href="index.html">Home Page</a></li>
                        <li><a href="examples.html">Style Examples</a></li>
                        <li><a href="#">Commodo vestibulum sem mattis</a></li>
                        <li><a href="#">Sed aliquam libero ut velit bibendum</a></li>
                        <li><a href="#">Maecenas condimentum velit vitae</a></li>
                    </ul>
                </aside>
            </div><! --/row -->
        </div> <!-- /container -->
        <footer>
            <div class="container">
                <div class="footer-content">
                    &copy; 2017
                </div>
            </div>
        </footer>
    </body>
</html>
