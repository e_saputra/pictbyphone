<div class="container">
    <div class="row">
        <content class="col-md-8">
            <section>
                <form class="form-horizontal" action="<?php echo base_url()?>forum/upload_post" method="post" style="margin-top: 50px">
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="category">Pilih Category</label>
                        <div class="col-xs-8">
                            <select name="category" class="form-control">
                                <option value="">nanti ada isiya</option>
                            </select>
                        </div>
                        <div class="col-xs-5" style="color: red">
                        <?php // echo form_error('category'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="judul">Judul</label>
                        <div class="col-xs-8">
                            <input type="text" class="form-control" name="judul">
                        </div>
                        <div class="col-xs-5" style="color: red">
                        <?php echo form_error('judul'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3"  for="image">Image</label>
<!--                        <div class="col-xs-7">
                            <input type="file" name="gambar" style="border-right: none">
                        </div>-->
                        <div class="col-xs-8">
                        <div class="input-group">
                            <label class="input-group-btn">
                                <span class="btn btn-primary">
                                    Browse&hellip; <input type="file" style="display: none;" multiple>
                                </span>
                            </label>
                            <input name="image" type="text" class="form-control" readonly size="7">
                        </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3"  for="detail">Detail</label>
                        <div class="col-xs-8">
                            <textarea class="form-control" name="content" rows="20" style="resize: none"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3"  for="detail"></label>
                        <div class="col-xs-8">
                            <input type="submit" class="btn btn-primary" value="submit">
                        <input type="button" class="btn btn-primary" value="preview">
                        </div>
                    </div>
                </form>
            </section>
        </content>
        <script>
            $(function() {

  // We can attach the `fileselect` event to all file inputs on the page
  $(document).on('change', ':file', function() {
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);
  });

  // We can watch for our custom `fileselect` event like this
  $(document).ready( function() {
      $(':file').on('fileselect', function(event, numFiles, label) {

          var input = $(this).parents('.input-group').find(':text'),
              log = numFiles > 1 ? numFiles + ' files selected' : label;

          if( input.length ) {
              input.val(log);
          } else {
              if( log ) alert(log);
          }

      });
  });
  
});
</script>